import socket


HOST = '127.0.0.1'
PORT = 65432
requestTypes = ['100', '200', '300', '400', '500', '600', '700', ]

def greeting(conn):
    message = 'hello'
    conn.sendall(message.encode())


'''
read the file and split it into a dictionary of lists made up of
album,song,length,lyrics
'''

def handle_DB():
    #currently MOCKUP DATA
    db = {'album1': [{'song1': ['15:12', 'never gonna give you up']}, {'song2': ['15:13', 'never gonna ley you down']}, {'song3': ['3:12', 'never gonna run around and desert you']}], 'album2': [
        {'song12': ['15:12', 'never gonna give you up']}, {'song21': ['15:13', 'never gonna ley you down']}, {'song312': ['3:12', 'never gonna run around and desert you']}]}
    return db


def process_request(message, db, conn):
    '''
    request example:
    100:REQUEST:command=get_album_list&param{‘’}”

    get the &param=
    '''
    request_type = message.split(':')[0]
    param = message.split(':')[2].split('&')[1].split('=')[1]

    if request_type in requestTypes:
        if request_type == '100':
            response = get_album_list(db)
        elif request_type == '200':
            response = get_song_list(param, db)
        elif request_type == '300':
            response = get_song_length(param, db)
        elif request_type == '400':
            response = get_song_lyrics(param, db)
        elif request_type == '500':
            response = get_song_album(param, db)
        elif request_type == '600':
            response = find_song_name(param, db)
        elif request_type == '700':
            response = find_song_by_lyrics(param, db)
        elif request_type == '800':
            response = 'close'
        else:
            print('invalid request')
        return response


''''
request example:
100:REQUEST:command=get_album_list&param{‘’}”

return "200:OK:&album_list={‘album1,album2,album3,album4,’}"
'''

def get_album_list(db):
    '''
    proccess by album in dict
    '''
    album_list = []
    for album in db:
        album_list.append(album)
    return '200:OK:&album_list={' + ','.join(album_list) + '}'


def getKeys(album, db):
    all_keys = set().union(*(d.keys() for d in db[album]))
    return all_keys


def get_song_list(album, db):
    return '200:OK:&song_list={' + ','.join(getKeys(album, db)) + '}'


def get_song_length(song, db):
    # find song in every album
    for i in db:
        for j in db[i]:
            if song in getKeys(i, db):
                return '200:OK:&song_length=2:15'


def get_song_lyrics(song, db):
    for i in db:
        for j in db[i]:
            if song in getKeys(i, db):
                return '200:OK:&song_lyrics=never gonna give you up'


def get_song_album(song, db):
    '''
    find string in array
    '''
    for i in db:
        if song in getKeys(i, db):
            return '200:OK:&song_album=album1'


def find_song_name(song, db):
    '''
    search for song name even partly and return array of possible matches
    '''
    song_name = []
    for song in db:
        if song.find(song) != -1:
            song_name.append(song)
    return '200:OK:&song_name={' + ','.join(song_name) + '}'


def find_song_by_lyrics(lyrics, db):
    for i in db:
        if lyrics in db[i]['lyrics']:
            return '200:OK:&song_by_lyrics= song2'


def close_connection(conn):
    conn.close()


def main():
    '''
    if connection is made send message 'hello' to client
    '''
    db = handle_DB()
    while True:
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.bind((HOST, PORT))
            s.listen()
            conn, addr = s.accept()
            with conn:
                greeting(conn)
                while True:
                    data = conn.recv(1024)
                    if not data:
                        break
                    if data.decode().startswith('800:REQUEST'):
                        conn.close()
                        print("bye")
                        quit()
                    print('Received', data.decode())
                    '''
                    send to client response
                    '''
                    response =process_request(data.decode(),db,conn)
                    conn.sendall(response.encode())



if __name__ == '__main__':
    main()
