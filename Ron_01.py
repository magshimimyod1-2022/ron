import requests
NAHMAN_URL = 'http://webisfun.cyber.org.il/nahman'
NAHAMAN_WORDS_URL = 'http://webisfun.cyber.org.il/nahman/final_phase/words.txt'


def extract_password_from_site():
    '''
    get the 100'th char in the site's files from 11 to 34  and combine them
    :return: string

    '''
    password = ''
    for i in range(11, 35):
        r = requests.get(NAHMAN_URL + "/files/file"+str(i) + '.nfo')
        password += str(r.content)[101]
    return password

def find_most_common_words(url,num_of_words_in_sentence):
    '''
    get the 6 most common words from the site and make a sentence by spliting them with a space and return it
    :param url:
    :param num_of_words_in_sentence:
    :return: list of strings
    '''
    r = requests.get(url)
    words = r.text.split()
    words_count = {}
    for word in words:
        if word in words_count:
            words_count[word] += 1
        else:
            words_count[word] = 1
    words_count = sorted(words_count.items(), key=lambda x: x[1], reverse=True)
    return ' '.join([word[0] for word in words_count[:num_of_words_in_sentence]])

def user_menu():
    '''
    print the menu and get the user's choice
    :return: int
    '''
    print('\n')
    print('1. Get the password')
    print('2. Get the 6 most common words')
    # print('3. Exit')
    return int(input('Enter your choice: '))

def main():
    choice = user_menu()
    while choice != 3:
        if choice == 1:
            print(extract_password_from_site())
        elif choice == 2:
            print(find_most_common_words(NAHAMAN_WORDS_URL, 6))
        choice = 3

if __name__ == '__main__':
    main()