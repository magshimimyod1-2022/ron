
import socket

SERVER_IP = "54.71.128.194"
SERVER_PORT = 92
MY_SERVER_IP = ""
MY_SERVER_PORT = 9090


def proccess_response(response):
    # maniputlate response to format - add "." before "jpg"
    response = response.decode()
    response = response.replace("jpg", ".jpg")
    if "SERVERERROR" in response:
        # replace everything before "#" with "ERROR"
        response = response.replace(response[:response.find("#")], "ERROR")
    # if response contains France - return 'ERROR#"France is banned!"'
    if "france" in response.lower():
        response = 'ERROR#"France is banned!"'

    # if a actor is in the response return the actor name. list of actors in blacklist.txt split by ,
    with open("blacklist.txt", "r") as f:
        blacklist = f.read().split(",")
    for actor in blacklist:
        if actor.lower() in response.lower():
            response = 'ERROR#"Movie is Banned due to {} playing in the movie"'.format(actor)
            break
    return response.encode()


def main():
    # create a socket
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.bind((MY_SERVER_IP, MY_SERVER_PORT))
        s.listen(1)
        conn, addr = s.accept()
        with conn:
            print(f'Connected by {addr} \n')
            while True:
                data = conn.recv(1024)
                if not data:
                    break
                print(f"Received data from client:\n{data.decode()} \n")
                with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
                    s.connect((SERVER_IP, SERVER_PORT))
                    s.sendall(data)
                    response = s.recv(1024)
                    print(f"Received data from server:\n{response.decode()}\n")
                    response = proccess_response(response)
                    print(f"Sending fixed data to client:\n{response.decode()} \n")
                    conn.sendall(response)


if __name__ == "__main__":
    print("\nblacklist.txt is separated by commas!\n")
    print("Proxy server started\n")
    print("Close window to quit\n")
    while True:
        main()
