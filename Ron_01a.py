def physic_ok(behavior):
    physic = input("What is the physical equivalent of {}?\n".format(behavior))
    physic_op = input('Do you consider the behavior "{}" ok? please write "yes" or "no"\n'.format(physic))
    return physic_op.lower() == 'yes'


def moral(behavior):
    '''
    ask how would they feel if they did the behavior
    '''
    moral_op = input('How would you feel if someone "{}"ed you? please write "good" or "bad"\n'.format(behavior))
    return moral_op.lower() == 'good'

    








def main():
    behavior = input("Welcome to the moral decision making assistant! What is the behavior in question?\n")
    physic_answer = physic_ok(behavior)
    if not(physic_answer):
        print ("According to our analysis, you should not to it. Try to find other ways to solve your problem.")
    else:
        is_moral = moral(behavior)
        if is_moral:
            print ("According to our analysis, you should do it.\nHave a great day!")
        else:
            print ("According to our analysis, you should not to it. Try to find other ways to solve your problem.")


if __name__ == "__main__":
    main()
